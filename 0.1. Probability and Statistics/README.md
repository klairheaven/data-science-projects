This section will contain the basic concepts of Probability and Statistics needed for Data Science (from interview perspective). The folder contain detailed explanation of the same concepts along with solved questions of those concepts.

This readme file will only contain a breif introduction to the topics in the folders.

# Probability 

Probability is a real number between 0 and 1 that represents the likelihoof of that something is going to occur.

**Random Experiment :** is an experiment with more than one outcome

**Sample Size :** Sometimes called probability space or universal set denoted as $`\mathcal{S}`$ is the collection of all possible outcomes within the random experiment.

**Some Theorems**

1. $`P(A') = 1 - P(A)`$
2. $`P(\emptyset) = 0 `$
3. If $`A \subset B`$, then $`P(A) \leq P(B)`$
4. $`P(A \cup B) = P(A) + P(B) - P(A \cap B)`$

Point 4 is equivalent to $`P(\text{atleast}) = 1 - P(\text{none})`$.

## <u> Series Review </u>

<u>**Geometric Series**</u>

```math
\begin{equation}
\begin{split}
\sum_{k=0}^{\infty} b^k = \frac{\text{first term} }{1 - \text{base}}, \qquad |b| < 1 \\
\sum_{k=m}^{\infty} b^k = \frac{b^m}{1 - \text{base}} , \qquad |b| < 1 
\end{split}
\end{equation}
```

<u>**Exponential Series**</u>
```math
\begin{equation}
\begin{split}
\sum_{k=0}^{\infty} \frac{a^k}{k!} = e^a \\
\sum_{k=1}^{\infty} \frac{a^k}{k!} = e^a - 1\\
\sum_{k=2}^{\infty} \frac{a^k}{k!} = e^a - 1 - a
\end{split}
\end{equation}
```


## <u> Conditional Probability </u>

```math
P(A|B) = \frac{P(A \cap B)}{P(B)}
```
![Representation of Conditional Probability](images/conditional_probability.jpeg "Representation of Conditional Probability")

## <u> Multiplication Law of Probability </u>

```math
P(A \cap B) = P(B) \cdot P(A|B)\\
P(A \cap B) = P(A) \cdot P(B|A)\\
```

## <u> Generalizatin of Multiplication Law of Probability </u>
```math
\begin{equation}
\begin{align*}
P(A \cap B \cap C) & = P(A) \cdot P(B|A) \cdot P(C|A\cap B) \\
& = \cancel{P(A)} \cdot \frac{ \cancel{P(A \cap B) }}{ \cancel{P(A)} } \cdot \frac{ P(C \cap A \cap B) }{ \cancel{P(A \cap B) } } \\
& = P(C \cap A \cap B) \qquad \text{true}
\end{align*}
\end{equation}
```

## <u> Total Law of Probability </u>

```math
P(A) = \sum_{i=1}^{m} P(A \cap B_i) = \sum_{i}^{m} P(B_i) \cdot P(A | B_i)
```

## <u> Baye's Therorem </u>
```math
P(B|A) = \frac{ P(A\cap B) }{ P(A) } \Rightarrow P(B|A) = \underbrace{ \frac{ P(B) \cdot P(A|B) }{P(B) \cdot P(A|B) + P(B') \cdot P(A|B') } }_{\text{Baye's Theorem}}
```

## <u> General Form of Baye's Therorem </u>

```math
P(B_i|A) = \frac{ P(B_i) \cdot P(A|B_i) }{ \sum_{i=1}^{m} P(B_i) \cdot P(A|B_i) }
```
## <u> Independent Events </u>

If A and B are independent if and only if: 
```math
P(A) = P(A|B) \qquad P(B) = P(B|A) \qquad P(A\cap B) = P(A)\cdot P(B)
```
Checking just one of these satisfy the rest! 

## <u> Multiplication Law 1 </u>
$`P(A \cap B) = P(A) \cdot P(B|A) \qquad`$ (Any event)

$`P(A \cap B) = P(A) \cdot P(B) \qquad`$ (Independent Event) 

## <u> DeMorgan's Law of Probability </u>
```math
\begin{equation}
\begin{align*}
P(\text{atleast 1 }A_i \text{ occurs}) & = 1 - P(\text{none of }A_i \text{ occurs})\\
P( A_1 \cup A_2 \cup \ldots \cup A_n ) & = 1 - P(  A_1' \cap A_2' \cap \ldots \cap A_n'  ) \\
\text{(if independent)} & = 1 - P(A_1') P(A_2') \ldots  P(A_n')
\end{align*}
\end{equation}
```

## <u> Permutation and Combination </u>

**Permutations** are the possible oredered selections of r objects out of of a total of n objects.
```math
^nP_r = \frac{ n! }{ (n-r)! }
```

**Combination** are the possible selections of r items from a group of n items regardless of the order of selection
```math
^nC_r = \frac{ n! }{ r!(n-r)! } = \frac{^nP_r}{r!}
```

**Properties of Combination**
```math
\begin{pmatrix} n\\ r \end{pmatrix} = 
\begin{pmatrix} n\\ n-r \end{pmatrix} = 
\frac{ n! }{ r!(n-r)! }
```
```math
\begin{pmatrix} n\\ 0 \end{pmatrix} = 
\begin{pmatrix} n\\ n \end{pmatrix} = 
\frac{ n! }{ n!(n-0)! } = 1
```
```math
\begin{pmatrix} n\\ 1 \end{pmatrix} = 
\begin{pmatrix} n\\ n-1 \end{pmatrix} = n
```
```math
\begin{pmatrix} n\\ 2 \end{pmatrix} = 
\begin{pmatrix} n\\ n-2 \end{pmatrix} = 
\frac{ n(n-1) }{ 2 }
```
```math
\underline{\textbf{Binomial Theorem: }} \qquad (a+b)^n = \sum_{0}^{\infty}  \begin{pmatrix} n\\ k \end{pmatrix} a^kb^{n-k}
```
```math
\sum_{0}^{n}  \begin{pmatrix} n\\ k \end{pmatrix} = 2^n \qquad \sum_{0}^{n}  (-1)^k \begin{pmatrix} n\\ k \end{pmatrix} = 0
```

# Statistics

## <u> Discrete Random Variable </u>

A  **random variable** associates a numerical value with each outcome of a radom experiment.

A random variable is said to be discrete if it has either a finite number of values or inifinetly many values that can be arranged in a sequence.

If a random variable represents some measurement on a continuous scale and therefore capable of assuming all values in an interval, it is called **continuous** random variable.

The **Probability Distribution** of a discrete random variable is a list of all its distinct numerical values along with their associated probabilities:
![Example of Prob Distribution](images/prob_distribution_example.jpeg "Example of Prob Distribution")

*Definition*: A discrete distribution or the distribution or the **probability mass function (p.m.f)** of a discrete random variable is described by giving its probability function, that is giving the value
```math
f(x) = p_x(k) = P(X=k)\quad  \text{ for all values of }k\\
\text{ The p.m.f of r.v. X satisfies}:\\
\begin{align}
1.)& \text{ For each } x: \ 0 \leq f(x) \leq 1\\
2.)& \sum_{\text{all }x} f(x) = 1 \qquad 
\end{align}
```
Given a random variable $`X`$ with *p.m.f*
```math
f(x) = p_x(x) = P(X = X)
```
The **Mean** of a discrete *r.v* X is 
```math
E(X) = \mu_x = \sum_{\text{all x}} x \cdot f(x)
```
The **Variance** of a discrete *r.v* X is 
```math
\text{Var}(X) = \sigma_X^2 = E[(x - \mu_X)^2] = \sum_{\text{all x}}(x-\mu_X)^2
f(x)
```

Equivalent formula which is useful for computations is 
```math
\text{Var}(X) = \sigma_X^2 = E(X^2) - [E(X)]^2\\
= \sum_{\text{all x}}x^2 \cdot f(x) - [E(X)]^2
```
The **Standard Deviation** of X is the square root of Variance of X:
```math
SD(X) = \sigma_X = \sqrt{\sigma_X^2}
```
Intuitively, *S.D(X)* should be understood as a measure of how spread out the distribution of X is around its mean $`\mu`$, it gives a rough idea of the typical size of the absolute deviation $`|X-\mu|`$.

The **Cumulative Distribution Function** (*c.d.f*) $`F_X(x)`$ for a random variable is
```math
F_X(x) = P(X \leq x)
```

**Expectation of a function X**
```math
E(g(x)) = \sum_{\text{all x}} g(x) \cdot f(x)
```

**Properties of Expectation, Variance, SD**
First 
```math
E(c) = c \sum_{x} c \cdot f(x) = c \sum_{x} f(x) = c\cdot 1 = c
```
Second 
```math
\begin{align}
E(ax+b) & = c \sum_{x} (ax + b) \cdot f(x) \qquad \text{where a, b are constant}\\
& = a \sum_x x \cdot f(x) + b \cancel{\sum_{x} f(x)}\\
E(ax + b) & = a \cdot E(X) + b
\end{align}
```
Thus 
```math
\boxed{
E(ax + b) & = a \cdot E(X) + b
}
```

Third
```math
\boxed{
\text{Var}(Y = aX + b) = a^2 \text{Var}(X)
}
```


## <u> Bernoulli Trail (Bernoulli Experiment) </u>

A Bernoulli experiment is when repeated trails satisfy:
* Each trail results in one of the two outcome, called Success (S) and failure (F). 
* The Probability of success on a single trail is equal to *p* and remains the same from trail to trail.
* The trails are independent.

Bernoulli trails give rise to a Bernoulli Random Variable: 
```math
X(\text{success}) = 1 \qquad X(\text{fail}) = 0 \\
X \sim \text{Bernoulli}(P)
```

**Mass Function for Bernoulli**
```math
f(x) = p^x(1-p)^{1-x} \qquad x = 0,1
```
**Expectation, Variance, SD for Bernoulli**
```math
E(x) = p \qquad \text{Var}(X) = p(1-p)\\
E[X] = \sum_{\text{all x}} x\cdot f(x) = 0 (1-p) + p p
```
```math
\text{Var}[X] = E[X^2] - (E[X])^2 = p(1-p)
```

## <u> Binomial Distribution </u>
Repeated trails in which
* The number of trails, n, is fixed.
* Each trail is either a "Success" or a "failure".
* The Probability of "Success" p is same from trail to trail.
* The trails are independent.
```math
\text{X} = \text{number of "success" in n independent trails}
\text{Then} X \sim Bin(n,p)
```
```math
P(X = k) = \begin{pmatrix} n \\ k \end{pmatrix} \cdot p^k (1-p)^{n-k} - _nC_k \cdot p^k \cdot (1-p)^{n-k}
```
where $`k=0,1,2,3,\ldots, n`$
```math
E(X) = n\cdot p \qquad \text{Var}(X) = n \cdot p (1-p) \qquad \text{SD}(X) = \sqrt{n\cdot p \cdot (1-p)}
```

## <u> Geometric Distribution </u>
X = The number of independent trails until the first "success" (in Bernoulli(p) trails)

= the number of the trail on which the first success occurs.

Then $`X = \text{Geom}(p)`$

```math
P(X=x) = (1-p)^{x-1}\cdot x, \qquad x =1,2,3,\ldots\\
E(X) = \frac{1}{p} \qquad \text{Var}(X) = \frac{1-p}{p^2}
```

If $`a`$ is a non-negative integer.
```math
\begin{align}
P(X > a) & = f(a+1) + f(a+2) + f(a+3) + \ldots \\
& = (1-p)^a p + (1-p)^{a+1} p + (1-p)^{a+2} p + \ldots\\
& = \frac{ (1-p)^a p  }{ 1 - (1-p)^a }\\
& = (1-p)^a\\
P(X>a) & = 1 - \text{cdf}(a)
\end{align}
```

**Memoryless Property for Geom. Distribution**

If a, b are two positive integers
```math
\begin{align}
P(x>a+b | x > a) = P(x>b)
\end{align}
```


## <u> Negative Binomial Distribution </u>

$`X_k` = $ the number of independent trials until the $`k^{th}`$ "success" in Bernoulli(p) trails.

Then $`X_k \sim N\ B(k,p)`$ or $`X \sim N\ B(p)`$
```math
\begin{align}
P(X=x) = \begin{pmatrix} x-1 \\ k-1 \end{pmatrix} \cdot p^k \cdot (1-p)^{x-k}, \ x=k, k+1, k+2, \ldots
\end{align}
```


## <u> Named Discrete Distribution </u>

## <u> Hypergeometric Distribution </u>

N = Population Size

S = number of "successes" in the population

N-S = number of failures in the population

n = sample size

X = number of successes in the same when the sampling is done without replacement.

```math
\begin{align}
P(X = x) = \frac{ \begin{pmatrix} S \\ x \end{pmatrix} \cdot \begin{pmatrix} N-S \\ n-x \end{pmatrix}  }{ \begin{pmatrix} N \\ n \end{pmatrix}} = \frac{ _SC_x \cdot _{N-S}C_{n-x}  }{ _NC_n }
\end{align}
```

![Table of binomial dist vs hypergeometric dist](images/binomial_vs_hypergeometric_distribution.jpeg "Table of binomial distribution vs hypergeometric distribution")

If the population size is large (compared to the sample size) Binomial Distribution can be used regardless of whether sampling is with or without replacement.


## <u> Multinomial Distribution </u>

* The number of trails, n, is fixed.
* Each trail has $`k`$ possible outcomes, with probabilities $`p_1,\ p_2,\ldots,\ p_k`$ ($`p_1 + p_2 + \ldots + p_k = 1`$)
* The trails are independent.
* $`X_1,\ X_2, \ldots ,\ X_k`$ represents the number of times outcomes 1, outcome 2, $`\ldots`$, outcome k occur, respectively ($`X_1 + X_2 + \ldots + X_k = n`$)

Then
```math
\begin{align}
P(X_1 = x_1, X_2 = x_2, \ldots , X_k = x_k) = \frac{n!}{x_1! x_2!\ \ldots\ x_k!} p_1^{x_1}\ p_2^{x_2} \ \ldots p_k^{x_k}
\end{align}
```

## <u> Poisson Distribution </u>

X = the number of occurences of a particular event in an interval of time or space.
```math
\begin{align}
P( 0 \text{ occurences} ) & = 1 - \lambda h \\
P( 1 \text{ occurences} ) & = \lambda h \\
P( 2 \text{ or more occurences} ) & = 0 \\
\end{align}
```
![](images/poisson_distribution.jpeg "Example of Prob Distribution")


```math
\begin{align}
& \lim_{n \to \infty} P\bigg[ K \text{occurences on } [0,t]\bigg] \approx \begin{pmatrix} n \\ k \end{pmatrix} \bigg (\frac{\lambda t}{n} \bigg)^k \bigg( 1- \frac{\lambda t}{n} \bigg)^{n-k} \\
& = \lim_{n \to \infty} \frac{ n(n-1) \ldots (n-k+1) }{n \cdot n \cdot \ldots \cdot n} \frac{(\lambda t)^k}{k!} \bigg( 1- \frac{\lambda t}{n} \bigg)^n \cdot \bigg( 1- \frac{\lambda t}{n} \bigg)^{-k}\\
& \lim_{n \to \infty} \bigg( 1 + \frac{a}{n} \bigg)^{n} = e^a 
\end{align}
```

The equation becomes 

```math
\begin{align}
1 \cdot \frac{(\lambda t)^k}{k!} e^{-\lambda t}
\end{align}
```

```math
\begin{align}
P(X = x) = \frac{ \lambda^x \cdot e^{- \lambda} }{ x! }, \qquad x =0,1,2,3, \ldots
\end{align}
```

```math
\begin{align}
E[X] & = \sum_{\text{all }x} x \cdot P[X = x] \\
& = \sum_{x=0}^{\infty} x \cdot \frac{\lambda ^x \cdot e^{-\lambda}}{ x! } \\
& = \lambda e ^{-\lambda} \sum_{x=1}^{\infty} x \cdot \frac{\lambda ^{x-1} \cdot e^{-\lambda}}{ (x-1)! }\\
& = \lambda e ^{-\lambda} \sum_{x-1=0}^{\infty} x \cdot \frac{\lambda ^{x-1} \cdot e^{-\lambda}}{ (x-1)! }\\
& = \sum_{n=0}^{\infty} x \cdot \frac{\lambda ^{n} \cdot e^{-\lambda}}{ n! }\\
& = \lambda e ^{-\lambda} e^{\lambda}\\
E[X] & = \lambda
\end{align}
```

```math
\begin{align}
E[X] = \lambda \qquad \text{Var}[X] = \lambda
\end{align}
```

**Poisson Approximation for the Binomial Distribution**

*Rule of Thumb:* If we have a large number of independent events with small probability then the numbers events that occur has approximately a Poisson distribution

When n is large ($`n \geq 20`$) and $`p`$ is small ($`p \leq 0.05`$) and $`n\cdot p \leq 5`$, Binomial probabilities can be approximated by Poisson probabilities.



$$
\begin{align}

\end{align}

$$